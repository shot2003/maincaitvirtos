package com.example.email

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth

class profileactivity : AppCompatActivity() {
    private lateinit var buttonLogout:Button
    private lateinit var buttonPasswordChange:Button
    private lateinit var textView:TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profileactivity)
        init()
        registerListeners()
        textView.text = FirebaseAuth.getInstance().currentUser?.uid
    }

    private fun init(){
        buttonLogout = findViewById(R.id.buttonlogout)
        buttonPasswordChange = findViewById(R.id.buttonchangepassword)
        textView = findViewById(R.id.textView)
    }

    private fun registerListeners(){
        buttonLogout.setOnClickListener(){
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this,LoginActivity::class.java))
            finish()
        }
        buttonPasswordChange.setOnClickListener(){
            startActivity(Intent(this,PasswordChangeActivity2::class.java))
        }
    }
}